let row1;
let column1;
let tableArr = [];
let newTableArr = [];
let activeTable = null;

const tableFirst = document.getElementById('tableFirst');
const body = document.querySelector('body');
const textarea1 = document.getElementById('textarea1');
const textarea2 = document.getElementById('textarea2');
const newTable = document.getElementById('tableSecond');

function sum() {
  let grows = document.getElementById('rows').value;
  let gcolumns = document.getElementById('columns').value;

  row1 = (+grows);
  column1 = (+gcolumns);

  textarea1.value = '';
  //textarea2.value = null;
  tableFirst.innerHTML = '';
  newTable.innerHTML = '';

  resetTextFunc();
  clearFunc();

  function funcImput() {
    document.getElementById('rows').value = '';
    document.getElementById('columns').value = '';
  }

  setTimeout(funcImput, 200);
}

document.getElementById('start').addEventListener('click', sum);

let count = 0;

/* Генерация таблицы */
// create table start
function createTable(parent, row1, column1) {

  let table = '<table id="table-1">';

  for (let f = 0; f < row1; f++) {
    tableArr.push([]);

    table += '<tr>';

    for (let p = 0; p < column1; p++) {
      tableArr[f].push({
        value: '',
        index: count
      });
      table += '<td></td>';
      count += 1;
    }
    table += '</tr>';
  }

  newTableArr = [...tableArr]

  table += '</table>';

  tableFirst.innerHTML = table;
}
// create table end

function clearTable() {
  const table = activeTable === 'second' ? tableFirst : newTable;
  let tdd = table.querySelectorAll('td');
  tdd.forEach(td => td.style.backgroundColor = '#FFFFFF');
}

// Навигация и ввод
function handKeyup(table) {
  let tdd = table.querySelectorAll('td');

  body.addEventListener('keyup', function(e) {


    let currentIndex = -1;
    tdd.forEach((el, i) => {
      if (el.style.backgroundColor === 'rgb(238, 238, 238)') {
        currentIndex = i;
      }
    });

    if (currentIndex === -1) return;

    if (e.keyCode === 39 || e.keyCode === 37 || e.keyCode === 40 || e.keyCode === 38) {
      console.log('currentIndex', currentIndex);
      tdd[currentIndex].style.backgroundColor = '#FFFFFF';

      const input = tdd[currentIndex].querySelector('input');
      if (input) {
        input.blur();
      }
    }

    //right
    if (e.keyCode === 39 && currentIndex >= 0) {
      tdd[currentIndex === tdd.length - 1 ? 0 : currentIndex + 1].style.backgroundColor = '#EEEEEE';
    }

    //left
    if (e.keyCode === 37 && currentIndex >= 0) {
      tdd[currentIndex === 0 ? tdd.length - 1 : currentIndex - 1].style.backgroundColor = '#EEEEEE';
    }

    //down
    if (e.keyCode === 40 && currentIndex >= 0) {
      if (currentIndex === tdd.length - 1) {
        tdd[0].style.backgroundColor = '#EEEEEE';
      } else if (currentIndex + column1 >= tdd.length) {
        tdd[column1 - (tdd.length - currentIndex) + 1].style.backgroundColor = '#EEEEEE';
      } else {
        tdd[currentIndex + column1].style.backgroundColor = '#EEEEEE';
      }
    }

    //up
    if (e.keyCode === 38 && currentIndex >= 0) {
      if (currentIndex === 0) {
        tdd[tdd.length - 1].style.backgroundColor = '#EEEEEE';
      } else if (currentIndex - column1 < 0) {
        tdd[tdd.length - (column1 - currentIndex) - 1].style.backgroundColor = '#EEEEEE';
      } else {
        tdd[currentIndex - column1].style.backgroundColor = '#EEEEEE';
      }
    }
  });
}

function handleBodyClick() {
  body.addEventListener('click', function() {
    activeTable = null;
    let cells = document.querySelectorAll('td');
    cells.forEach(td => td.style.backgroundColor = '#FFFFFF');
  });
}

function inputBlur(table) {
  let tdd = table.querySelectorAll('td');
  const currentTableArr = activeTable === 'second' ? newTableArr : tableArr;

  for (let x = 0; x < tdd.length; x++) {
    const input = tdd[x].querySelector('input');

    if (input) {
      input.addEventListener('blur', function(e) {
        e.stopPropagation();

        tdd.forEach((td, i) => {
          // const input = td.querySelector('input');

          if (x !== i) {
            td.style.backgroundColor = '#FFFFFF';
          } else {
            const value = input.value;
            currentTableArr.forEach(row => {
              const cell = row.find(cell => cell.index === i);
              if (cell) {
                cell.value = value;
              }
            });

            td.innerHTML = value;
            td.style.backgroundColor = '#FFFFFF';
          }
        });
      });
    }
  }
}

function handleCellClick(table, currentTable) {
  let tdd = table.querySelectorAll('td');

  if (currentTable) {
    handKeyup(currentTable === 'second' ? newTable : tableFirst);
  }

  // cells
  for (let x = 0; x < tdd.length; x++) {

    tdd[x].addEventListener('click', function(e) {
      e.stopPropagation();

      activeTable = currentTable;

      tdd.forEach((td, i) => {
        const input = td.querySelector('input');
        if (!input && x !== i) {
          td.style.backgroundColor = '#FFFFFF';
        }

        if (input && x !== i) {
          const value = input.value;
          td.innerHTML = value;
          td.style.backgroundColor = '#FFFFFF';
        }
      });

      tdd[x].style.backgroundColor = '#EEEEEE';

      const value =  tdd[x].innerHTML;
      if (value.length > 0 && !value.includes('input')) {
        tdd[x].innerHTML = `<input type = "text" value="${value}" onfocus>`;
        inputBlur(table);
      }

      if (value.length === 0 && !value.includes('input')) {
        tdd[x].innerHTML = '<input type = "text" onfocus>';
        inputBlur(table);
      }

      clearTable();
    });
  }
}


function resetTextFunc() {
  createTable(tableFirst, row1, column1);
  handleBodyClick();
  inputBlur(tableFirst);
  handleCellClick(tableFirst, 'first');
}

// Передача данных из tableFirst в textarea1

let arr = [];
function getValue() {

  let TableList = [];
  let tr = document.getElementById('tableFirst').getElementsByTagName('tr');

  const data = JSON.stringify(tableArr);

  textarea1.value = data;

  console.log(arr);

}


let text2;
function getTxt() {
  let c1 = document.getElementById('textarea1').value;
  let d1 = document.getElementById('textarea2');
  d1.innerHTML = c1;

  text2 = d1.value;
}



function createNewTable(parent, row2, cells, tableData) {

  let table = '<table id="table-2">';

  for (let f = 0; f < tableData.length; f++) {

    table += '<tr>';

    for (let p = 0; p < tableData[0].length; p++) {

      table += `<td>${tableData[f][p].value}</td>`;
    }
    table += '</tr>';
  }

  table += '</table>';

  parent.innerHTML = table;

  inputBlur(newTable);
  handleCellClick(newTable, 'second');

}

function getImport() {
  const data = document.getElementById('textarea2').value;
  const tableData = JSON.parse(data);
  const row2 = tableData.length;
  const cells = row2 > 0 ? tableData.length : 0;

  console.log('колонка',tableData[0].length);
  console.log('ячейка',tableData.length);

  createNewTable(newTable, row2, cells, tableData);

  function funcSet() {
    document.getElementById('textarea2').innerHTML = '';
  }

  setTimeout(funcSet, 200);

}

function clearFunc(){
  document.getElementById('textarea1').value = '';
  document.getElementById('textarea2').innerHTML = '';
}

////////////////////////////////////////////////////////
window.onkeyup = function(e) {
  console.log(e.keyCode);


};

